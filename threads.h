#ifndef THREADS_H
#define THREADS_H

#include <iostream>
#include <future>
#include <map>

using namespace std;

map<int, float> get_nb_size_tested_for_timeout(std::function<void(int,std::promise<float>&)>f, float timeout, int max_thread);

//! (OBSOLETE) démo d'utilisation de la classe fraction
void demo_fraction();

//! démo d'utilisation de la classe fractMatrix
/*! Construit deux Matrices de taille Msize x Msize puis calcule leurs
 *  transposées respectives, leurs somme, leurs différence et leurs produit.
 * \brief demo_matrix
 * \param Msize Taille des matrices (Msize x Msise) utilisé par la démo
 * \param display affiche ou non les résultats dans la console
 */
void demo_matrix(int Msize, bool display);

//! démo d'utilisation de la classe fractVector
/*! Construit deux Vecteurs de longeur Vsize puis calcule leurs
 *  transposées respectives, leurs somme, leurs différence et leurs produit scalaire.
 * \brief demo_matrix
 * \param Vsize longueur des Vecteurs utilisés par la démo
 * \param display affiche ou non les résultats dans la console
 */
void demo_vector(int Vsize, bool display);

/*! fonction appelé comme thread pour demo_matrix gérant la promesse
 *  retournant le temps d'execution du thread
 * \param Msize longueur des Vecteurs utilisés par la démo
 * \param p promesse de point de temps de fin de thread
 */
void thread_demo_matrix(int Msize, std::promise<float>&p);

/*! fonction appelé comme thread pour demo_vector gérant la promesse
 *  retournant le temps d'execution du thread
 * \param Vsize longueur des Vecteurs utilisés par la démo
 * \param p promesse de point de temps de fin de thread
 */
void thread_demo_vector(int Vsize, std::promise<float>&p);

#endif // THREADS_H

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "maxsizefortimeoutthread.h"

#include <QDebug>
#include <QMessageBox>
#include <QtCharts>
#include <QCoreApplication>
#include <thread>
#include <map>
#include <cmath>

#include "include/Fraction.h"
#include "include/FractMatrix.h"
#include "include/FractVector.h"

int Nb_Threads = -1;
map <int, float>ResultsMatrix = {{0,0}};
map <int, float>ResultsVectors = {{0,0}};

QString DisplayFraction(Fraction Frac){
    int num = Frac.get_num();
    int den = Frac.get_den();
    return QString::number(num) + QString("/")+ QString::number(den);
}

QString DisplayMatrix(FractMatrix M){
    QString toDisplay = "\n[";
    for (int i=0;i<M.get_nb_rows();++i)
    {
        if (i!=0)
            toDisplay += " ";
        toDisplay += "[";
        for (int j=0;j<M.get_nb_columns();++j)
        {
            toDisplay += " " + DisplayFraction(M(i,j));
            if (j!=M.get_nb_columns()-1)
                toDisplay += ",";
        }
        toDisplay += "]";
        if (i!=M.get_nb_rows()-1)
            toDisplay += "\n";
    }
    toDisplay += "] \n";
    return toDisplay;
}

void SetWidgetHiddable(QWidget *W){
    QSizePolicy WSizePolicy = W->sizePolicy();
    WSizePolicy.setRetainSizeWhenHidden(true);
    W->setSizePolicy(WSizePolicy);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setCentralWidget(ui->MainWidgetTab);
    SetWidgetHiddable(ui->Vect_Vec_Button);
    ui->Vect_Vec_Button->hide();
    SetWidgetHiddable(ui->onGoingTestVector);
    ui->onGoingTestVector->hide();
    SetWidgetHiddable(ui->onGoingTestMatrix);
    ui->onGoingTestMatrix->hide();
    // connect(ui->runFractionButton, SIGNAL(clicked()), this, SLOT(on_runFractionButton_clicked())); AUTOMATED FROM Qt IN setupUI NOW!
}



MainWindow::~MainWindow()
{
    delete ui;
}

// ============================= Fraction tab ===============================

void MainWindow::on_runFractionButton_clicked(){
     ui->Fraction_results_TextBrowser->clear();
     int A_den = ui->den_A->value();
     if (A_den==0){
         ui->Fraction_results_TextBrowser->append("FRACTION 'A': UN DENOMINATEUR DE NE PEUT ETRE NULL!\n");
     }
     int B_den = ui->den_B->value();
     if (B_den==0){
         ui->Fraction_results_TextBrowser->append("FRACTION 'B': UN DENOMINATEUR DE NE PEUT ETRE NULL!\n");

     }
     if (A_den==0||B_den==0){
         ui->Fraction_results_TextBrowser->setTextColor(Qt::red);
         return;
     }
     else
         ui->Fraction_results_TextBrowser->setTextColor(Qt::black);
     int A_num = ui->num_A->value();
     int B_num = ui->num_B->value();
     Fraction A(A_num,A_den);
     Fraction B(B_num,B_den);
     int btn_id=0;
     btn_id = ui->selectFractionOperatorButtonGroup->checkedId();
     qDebug() << "Fraction: Id of the selected button: " << btn_id;
     A.reduce();
     B.reduce();
     ui->Fraction_results_TextBrowser->append(
                 "A = " + DisplayFraction(A) + "\n" +
                 "B = " + DisplayFraction(B) + "\n\n");
     switch (btn_id) {
     case -2:
         ui->Fraction_results_TextBrowser->append(
                     "A + B = " + DisplayFraction(A+B));
         break;

     case -3:
         ui->Fraction_results_TextBrowser->append(
                     "A - B = " + DisplayFraction(A-B));
         break;

     case -4:
         ui->Fraction_results_TextBrowser->append(
                     "A * B = " + DisplayFraction(A*B));
         break;
     case -5:
         ui->Fraction_results_TextBrowser->append(
                     "A / B = " + DisplayFraction(A/B));
         break;
     }
};

// ============================= Matrice tab ===============================

void MainWindow::on_runMatrixButton_clicked(){
    ui->Matrix_results_TextBrowser->clear();
    int A_liqnes = ui->Nb_lignes_A->value();
    int A_colones = ui->Nb_colones_A->value();
    int B_liqnes = ui->Nb_lignes_B->value();
    int B_colones = ui->Nb_colones_B->value();
    FractMatrix A(A_liqnes,A_colones), B(B_liqnes, B_colones);
    A.FillRandomly();
    B.FillRandomly();
    ui->Matrix_results_TextBrowser->append("A = " + DisplayMatrix(A) +
        "\nB = "+DisplayMatrix(B));
    int btn_id=0;
    btn_id = ui->selectMatrixOperatorButtonGroup->checkedId();
    qDebug() << "Matrix: Id of the selected button: " << btn_id;
    try{
        switch (btn_id) {
            case -2:
                ui->Matrix_results_TextBrowser->append(
                            "A + B = " + DisplayMatrix(A+B));
                break;

            case -3:
                ui->Matrix_results_TextBrowser->append(
                            "A - B = " + DisplayMatrix(A-B));
                break;

            case -4:
                ui->Matrix_results_TextBrowser->append(
                            "A * B = " + DisplayMatrix(A*B));
                break;
         }
    }
    catch(exception& ex){
        ui->Matrix_results_TextBrowser->append(
                    "\n\n<span style=\" color: #ff0000;\">" +
                    QString(ex.what()) + "</span>");
    }
}

// ============================= Vector tab ===============================

void MainWindow::on_runVectorButton_clicked(){
    ui->Vector_results_TextBrowser->clear();
    int taille_A = ui->Taille_Vect_A->value();
    int taille_B = ui->Taille_Vect_B->value();
    FractVector A(taille_A), B(taille_B);
    A.FillRandomly();
    B.FillRandomly();
    ui->Vector_results_TextBrowser->append("A = " + DisplayMatrix(A) +
        "\nB = "+DisplayMatrix(B));
    int btn_id=0;
    btn_id = ui->selectVectorOperatorButtonGroup->checkedId();
    qDebug() << "Vector: Id of the selected button: " << btn_id;
    try{
        switch (btn_id) {
            case -2:
                ui->Vector_results_TextBrowser->append(
                            "A + B = " + DisplayMatrix(A+B));
                break;

            case -3:
                ui->Vector_results_TextBrowser->append(
                            "A - B = " + DisplayMatrix(A-B));
                break;

            case -4:
                ui->Vector_results_TextBrowser->append(
                            "transpose(A) * B = " + DisplayMatrix(A.T()*B));
                break;
            case -5:
                ui->Vector_results_TextBrowser->append(
                            "A . B = " + DisplayFraction(scalarProduct(A,B)));
                break;
            case -6:
                ui->Vector_results_TextBrowser->append(
                            "A ^ B = " + DisplayMatrix(A^B));
                break;
         }
    }
    catch(exception& ex){
        ui->Vector_results_TextBrowser->append(
                    "\n\n<span style=\" color: #ff0000;\">" +
                    QString(ex.what()) + "</span>");
    }
}

void MainWindow::on_Taille_Vect_A_valueChanged(){
    this->HideShowVectorialProcutChoice();
}

void MainWindow::on_Taille_Vect_B_valueChanged(){
    this->HideShowVectorialProcutChoice();
}

void MainWindow::HideShowVectorialProcutChoice(){
    if(ui->Taille_Vect_A->value()==3 && ui->Taille_Vect_B->value()==3)
        ui->Vect_Vec_Button->show();
    else{
        ui->Vect_Vec_Button->hide();
        if (ui->Vect_Vec_Button->isChecked())
            ui->Add_Vec_Button->setChecked(true);
    }
}

// ============================= Threading tab ===============================

void MainWindow::on_getMaxThreadsButton_clicked(){
    Nb_Threads = thread::hardware_concurrency()-1;
    ui->maxThreadsTextBrowser->clear();
    ui->maxThreadsTextBrowser->append(QString::number(Nb_Threads));
};

void MainWindow::on_timeOutMatrixSpinBox_valueChanged(){
    ui->displayThreadsMatrixButton->setEnabled(false);
}

void MainWindow::on_timeOutVectorSpinBox_valueChanged(){
    ui->displayThreadsVectorButton->setEnabled(false);
}

void MainWindow::on_runThreadsMatrixButton_clicked(){
    if(Nb_Threads==-1){
        QMessageBox msgBox;
        msgBox.setText("Déterminez le nombre max de threads concurrents possibles\npar votre sytème avant de lancer cette procédure");
        msgBox.exec();
        return;
    }
    ui->timeOutMatrixSpinBox->setEnabled(false);
    ui->onGoingTestMatrix->setVisible(true);
    qDebug() << "Go Matrix thread test";
    float timeout_matrice = ui->timeOutMatrixSpinBox->value();
    QCoreApplication::processEvents();
    MaxSizeForTimeOutThread *Matrix_Threads = new MaxSizeForTimeOutThread(1,timeout_matrice,Nb_Threads);
    connect(Matrix_Threads, &MaxSizeForTimeOutThread::ThreadsMatrixResultsReady, this, &MainWindow::retrieveThreadsMatrix);
    Matrix_Threads->start();
}

void MainWindow::retrieveThreadsMatrix(const std::map<int, float> &res){
    ResultsMatrix = res;
    ui->onGoingTestMatrix->setVisible(false);
    ui->displayThreadsMatrixButton->setEnabled(true);
    ui->timeOutMatrixSpinBox->setEnabled(true);
    qDebug() << ResultsMatrix;
};


void MainWindow::on_runThreadsVectorButton_clicked(){
    if(Nb_Threads==-1){
        QMessageBox msgBox;
        msgBox.setText("Déterminez le nombre max de threads concurrents possibles\npar votre sytème avant de lancer cette procédure");
        msgBox.exec();
        return;
    }
    ui->timeOutVectorSpinBox->setEnabled(false);
    ui->onGoingTestVector->show();
    qDebug() << "Go Vector thread test";
    float timeout_vector = ui->timeOutVectorSpinBox->value();
    QCoreApplication::processEvents();
    MaxSizeForTimeOutThread *Vector_Threads = new MaxSizeForTimeOutThread(2,timeout_vector,Nb_Threads);
    connect(Vector_Threads, &MaxSizeForTimeOutThread::ThreadsVectorResultsReady, this, &MainWindow::retrieveThreadsVector);
    Vector_Threads->start();
}

void MainWindow::retrieveThreadsVector(const std::map<int, float> &res){
    ResultsVectors = res;
    ui->onGoingTestVector->hide();
    ui->displayThreadsVectorButton->setEnabled(true);
    ui->timeOutVectorSpinBox->setEnabled(true);
    qDebug() << ResultsVectors;
};

void MainWindow::on_displayThreadsMatrixButton_clicked(){
    float timeout_matrice = ui->timeOutMatrixSpinBox->value();
    QLineSeries *treshold = new QLineSeries();
    treshold->setName("Seuil timeout à ne pas dépasser");
    QBarSet *setresults = new QBarSet("Durée du thread");
    QStringList categories;
    map<int, float>::iterator it_mat = ResultsMatrix.begin();
    float max_val = 0;
    treshold->append(QPoint(-1,timeout_matrice));
    while(it_mat != ResultsMatrix.end()){
        *setresults <<it_mat->second/1000000000;
        if (it_mat->second/1000000000>max_val)
            max_val = (it_mat->second)/1000000000;
        categories << QString::number(it_mat->first);
        it_mat++;
    }
    treshold->append(QPoint(it_mat->first,timeout_matrice));
    QBarSeries *series = new QBarSeries();
    series->append(setresults);
    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->addSeries(treshold);
    chart->setTitle("Détermination de la taille maximale des Matrice pour un timeout de " + QString::number(timeout_matrice) +"s\n");
    chart->setAnimationOptions(QChart::SeriesAnimations);
    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->append(categories);
    axisX->setTitleText("facteur n (taille matrice n*n)");
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);
    treshold->attachAxis(axisX);
    QValueAxis *axisY = new QValueAxis();
    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);
    treshold->attachAxis(axisY);
    axisY->setRange(0,round(max_val)*1.1);
    axisY->setTitleText("Durée du thread (s)");
    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);
    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->resize(500,400);
    chartView->show();
}

void MainWindow::on_displayThreadsVectorButton_clicked(){
    float timeout_vector = ui->timeOutVectorSpinBox->value();
    QLineSeries *treshold = new QLineSeries();
    treshold->setName("Seuil timeout à ne pas dépasser");
    QBarSet *setresults = new QBarSet("Durée du thread");
    QStringList categories;
    map<int, float>::iterator it_vec = ResultsVectors.begin();
    float max_val = 0;
    treshold->append(QPoint(-1,timeout_vector));
    while(it_vec != ResultsVectors.end()){
        *setresults <<it_vec->second/1000000000;
        if (it_vec->second/1000000000>max_val)
            max_val = it_vec->second/1000000000;
        categories << QString::number(it_vec->first);
        it_vec++;
    }
    treshold->append(QPoint(it_vec->first,timeout_vector));
    QBarSeries *series = new QBarSeries();
    series->append(setresults);
    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->addSeries(treshold);
    chart->setTitle("Détermination de la taille maximale des vecteurs pour un timeout de " + QString::number(timeout_vector) +"s\n");
    chart->setAnimationOptions(QChart::SeriesAnimations);
    QBarCategoryAxis *axisX = new QBarCategoryAxis();
    axisX->append(categories);
    axisX->setTitleText("facteur n (vecteur de n lignes)");
    chart->addAxis(axisX, Qt::AlignBottom);
    series->attachAxis(axisX);
    treshold->attachAxis(axisX);
    QValueAxis *axisY = new QValueAxis();
    chart->addAxis(axisY, Qt::AlignLeft);
    series->attachAxis(axisY);
    treshold->attachAxis(axisY);
    axisY->setRange(0,round(max_val)*1.1);
    axisY->setTitleText("Durée du thread (s)");
    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);
    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chartView->resize(500,400);
    chartView->show();
}

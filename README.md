# demoCPP

Small project to show my basic skills with C++:
- basics: variables, functions, pointers, memory allocation
- Object Oriented Programing: objects, heritage
- threading: thread, pool, benchmarking(chrono/timeout)
- Qt GUI
- Doc with Doxygen

more later on...

-------------------------------------------------------------
# Description du fonctionnement de l'application

Le script lance une interface graphique Qt comportant 4 onglets: chaque onglet va démontrer le bon fonctionnement de diverses fonctions du programme comme décri ci-dessous:

1. Demo Fraction

création d'un objet fraction et des principaux outils algébriques qui vont avec (POO de base, pointeurs, surcharge opérateurs...)

2. Demo Matrix

Création d'objets matrices de fractions puis démonstration des bases de l'algèbres linéaires que l'on peut utiliser avec (pointeurs de classe)

3. Demo Vectors

Idem Matrix, mais pour des vecteurs avec 2 nouvelles méthodes spécifiques : produit scalaire et produit vectoriel (héritage)

4. Multithreading

Algorithme de démonstration de multi-threading :
Les 2 fonctions de démonstration du point 2 font une série de calculs algébriques : une pour les matrices, une pour les vecteurs.
Elles prennent pour paramètre la taille des vecteurs ou matrices à utiliser pour faire ces calculs.
Le but de cette partie du programme est de chercher à trouver la plus grande taille possible de vecteurs ou de matrices pour réaliser leurs fonctions de démonstration correspondantes dans un temps imparti (Timeout).
Les fonctions de démonstrations sont donc lancées avec des valeurs de taille de vecteurs ou matrices croissantes en parallèle afin de déterminer le plus rapidement possible cette fameuse valeur max.

-------------------------------------------------------------
# Note

L'API est pour le moment disponnible sous linux dans le dossier "./linux/"


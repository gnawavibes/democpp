#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <map>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

//!  Classe de la fenêtre principale du programme
/*!
  Fenêtre composé d'un onglet par démo: fraction, Matrice, Vecteurs et multithreading
*/

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    //! Constructeur
    MainWindow(QWidget *parent = nullptr);

    //! Destructeur
    ~MainWindow();

public slots:
    //! Slot lançant la démo fraction
    /*! récupère les numérateurs et dénominateurs des 2 fractions, construit les
     *  objets 'Fraction' correspondants, récupère l'opération à réaliser et affiche le calcul
     *  et son résultat dans l'afficheur.
     * \brief on_runFractionButton_clicked
     */
    void on_runFractionButton_clicked();

    //! Slot lançant la démo Matrice
    /*! récupère les dimenssions des 2 Matrices, construit objets 'FractMatrix' correspondants,
     *  les remplis aléatoirement, récupère l'opération à réaliser et affiche le calcul
     *  et son résultat dans l'afficheur.
     * \brief on_runFractionButton_clicked
     */
    void on_runMatrixButton_clicked();

    //! Slot lançant la démo Vector
    /*! récupère les dimenssions des 2 vecteur, construit objets 'FractVector' correspondants,
     *  les remplis aléatoirement, récupère l'opération à réaliser et affiche le calcul
     *  et son résultat dans l'afficheur.
     * \brief on_runFractionButton_clicked
     */
    void on_runVectorButton_clicked();

    //! Slot nécessaire à l'activation/désactivation de la possibilité de faire un produit vectoriel (si les 2 vecteurs sont de dimension 3 ou non)
    void on_Taille_Vect_A_valueChanged();

    //! Slot nécessaire à l'activation/désactivation de la possibilité de faire un produit vectoriel (si les 2 vecteurs sont de dimension 3 ou non)
    void on_Taille_Vect_B_valueChanged();

    //! Slot permettant de déterminer le nombre de thread concurents possible
    void on_getMaxThreadsButton_clicked();

    //! Slot désactivant l'accès aux résultats des tests "matrices" si les conditions de test ont changé
    void on_timeOutMatrixSpinBox_valueChanged();

    //! Slot désactivant l'accès aux résultats des tests "vecteurs" si les conditions de test ont changé
    void on_timeOutVectorSpinBox_valueChanged();

    //! Slot gérant le lancement du test sur les threads "matrice"
    /*! Récupère les paramètres de test fourni par l'utilisateur, affiche un message informant
     *  que les tests sont en cours et lance le thread pilotant les test: 'get_nb_size_tested_for_timeout'
     * \brief on_runThreadsMatrixButton_clicked
     */
    void on_runThreadsMatrixButton_clicked();

    //! Slot gérant le lancement du test sur les threads "vecteur"
    /*! Récupère les paramètres de test fourni par l'utilisateur, affiche un message informant
     *  que les tests sont en cours et lance le thread pilotant les test: 'get_nb_size_tested_for_timeout'
     * \brief on_runThreadsMatrixButton_clicked
     */
    void on_runThreadsVectorButton_clicked();

    //! Récupère les résultats d'un test sur les threads "matrice" et active la possibilité d'afficher ces résultats
    /*!
     * \brief retrieveThreadsMatrix
     * \param res map sous forme {int taille matrice; float temps du test} contenant les résultats du tests
     */
    void retrieveThreadsMatrix(const std::map<int, float> &res);

    //! Récupère les résultats d'un test sur les threads "vecteur" et active la possibilité d'afficher ces résultats
    /*!
     * \brief retrieveThreadsMatrix
     * \param res map sous forme {int longueur vecteur; float temps du test} contenant les résultats du tests
     */
    void retrieveThreadsVector(const std::map<int, float> &res);

    //! Slot permettant d'afficher les résultats du dernier test sur les threads "matrice"
    void on_displayThreadsMatrixButton_clicked();

    //! Slot permettant d'afficher les résultats du dernier test sur les threads "vecteur"
    void on_displayThreadsVectorButton_clicked();

private:
    Ui::MainWindow *ui;
    //!active/désactive le radiobutton pour faire un produit vectoriel
    void HideShowVectorialProcutChoice();
};
#endif // MAINWINDOW_H

#ifndef FRACTMATRIX_H
#define FRACTMATRIX_H
#include "Fraction.h"
#include <vector>

using namespace std;

//!  Classe pour les objets "FractMatrix": des matrices avec des fractions pour termes
/*!
  Cette classe permet de créer des objets "FractMatrix" et de réaliser
  les opérations d'algèbre linéaire classiques avec des matrices.
*/
class FractMatrix
{
    public:

        //! Constructeur pour FractMatrix
        /*!
          Génère une matrice de fractions nulles de dimension 'nb_rows' lignes et
          'nb_columns' colonnes
          \param nb_rows le nombre de lignes de la matrice
          \param nb_columns le nombre de colonnes de la matrice
        */
        FractMatrix(int nb_rows, int nb_columns);

        //! Constructeur de copie
        FractMatrix(FractMatrix const& A);

        //! Destructeur
        virtual ~FractMatrix();

        //! rempli la matrice de fractions aléatoires sur l'intervalle[-10,10]
        void FillRandomly();

        //! rempli la matrice de fractions nulles
        void FillWithZeros();

        //! rend l'object FractMatrix affichable avec 'cout'
        void display(std::ostream &out) const;

        //! Accesseur du nombre de lignes
        /*!
            \return le nombre de lignes (int)
        */
        int get_nb_rows() const;

        //! Accesseur du nombre de colonnes
        /*!
            \return le nombre de colonnes (int)
        */
        int get_nb_columns() const;


        //! Accesseur de la matrice
        vector<vector<Fraction *> > get_matrix() const;

        //! Modifie le nombre de lignes
        /*!
            \param nb_rows le nombre de lignes de la matrice
        */
        void set_nb_rows(int nb_rows);


        //! Modifie le nombre de colonnes
        /*!
            \param nb_columns le nombre de colonnes de la matrice
          */
        void set_nb_columns(int nb_columns);

        //! permet de modifier le terme de la ligne 'i', colonne 'j', via l'opérateur '()'
        /*! utilisation de l'opérateur() en lvalue
         * \brief operator ()
         * \param i numéro de la ligne
         * \param j numéro de la colonne
         */
        Fraction& operator()(int i, int j);

        //! permet d'accéder au terme de la ligne 'i', colonne 'j', via l'opérateur '()'
        /*! utilisation de l'opérateur() en rvalue
         * \brief operator ()
         * \param i numéro de la ligne
         * \param j numéro de la colonne
         * \return Fraction
         */
        Fraction operator()(int i, int j) const;

        //! Surcharge de l'opérateur d'incrémentation par une autre FractMatrix
        FractMatrix& operator+=(FractMatrix const& FractMatrix2);

        //! Surcharge de l'opérateur de décrémentation par une autre FractMatrix
        FractMatrix& operator-=(FractMatrix const& FractMatrix2);

        //! Transposé de la FractMatrix
        /*! Calcule la transposé de la FractMatrix
         * \return FractMatrix de la transposée
         */
        FractMatrix T();

    protected:
        //! initialisation du nombre de lignes et de colonnes
        int _nb_rows, _nb_columns;

        //! initialisation de la matrice
        vector<vector<Fraction *> > _matrix;

    private:

};

//! Surcharge de l'opérateur d'affichage pour afficher l'objet "FractMatrix"
std::ostream& operator<<(std::ostream& out, FractMatrix const& fract);

//! Surcharge de l'opérateur d'addition
FractMatrix operator+(FractMatrix const& A, FractMatrix const& B);

//! Surcharge de l'opérateur de soustraction
FractMatrix operator-(FractMatrix const& A, FractMatrix const& B);

//! Surcharge de l'opérateur de multiplication
FractMatrix operator*(FractMatrix const& A, FractMatrix const& B);

#endif // FRACTMATRIX_H

#ifndef FRACTVECTOR_H
#define FRACTVECTOR_H

#include "include/FractMatrix.h"
#include "include/Fraction.h"

//!  Classe pour les objets "FractVector": des vecteurs avec des fractions pour termes
/*!
  Héritant de la classe "FractMatrix", cette classe permet de créer des objets "FractVector" et de réaliser
  les opérations d'algèbre linéaire classiques avec des vecteurs comme le produit scalaire et le
  produit vectoriel.
*/
class FractVector : public FractMatrix
{
    public:
        //! Constructeur pour FractVector
        /*!
          Génère un vecteur de fractions nulles de longueur 'colsize'
          \param colsize longueur du vecteur
        */
        FractVector(int colsize);

        //! Destructeur
        virtual ~FractVector();

        //! permet de modifier le terme 'i' via l'opérateur '[]'
        /*! utilisation de l'opérateur '[]' en lvalue
         * \brief operator []
         * \param i index tu terme
         */
        Fraction& operator[](int i);

        //! permet de modifier le d'accéder au terme 'i' via l'opérateur '[]'
        /*! utilisation de l'opérateur '[]' en lvalue
         * \brief operator []
         * \param i index tu terme
         */
        Fraction operator[](int i) const;

    protected:

    private:
};

//! Calcule le produit scalaire de 2 vecteurs
Fraction scalarProduct(FractVector const& A, FractVector const& B);

//! Opérateur pour le produit vectoriel de 2 vecteurs (si ils sont de longeur 3)
FractVector operator^(FractVector const& A, FractVector const& B); // produit vectoriel

#endif // FRACTVECTOR_H

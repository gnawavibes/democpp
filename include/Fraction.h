#ifndef FRACTION_H
#define FRACTION_H
#include <iostream>

//!  Classe pour les objets "Fraction"
/*!
  Cette classe permet de créer des objets "Fractions" et de réaliser
  les opérations arithmétiques classiques sur des fractions.

  Dans un soucis de simplification, on ne considère ici
  que des numérateurs et dénominateur entiers (bien que potentiellement négatif
  ou nuls).
*/

class Fraction
{
    public:
        //! Constructeur pour fraction nulle
        /*!
          Génère la fraction '0/1'
        */
        Fraction();

        //! Constructeur pour fraction représentant un entier
        /*!
          Génère la fraction 'num/1'
          \param num l'entier à mettre sous forme de fraction
        */
        Fraction(int num);

        //! Constructeur pour fraction
        /*!
          Génère la fraction 'num/den
          \param num le numérateur
          \param den le denominateur
        */
        Fraction(int num, int den);

        //! Constructeur de copie
        Fraction(Fraction const& tocopy);

        //! rend l'object Fraction affichable avec 'cout'
        void display(std::ostream &out) const;

        //! Simplifie la fraction au maximum (toujours des entiers au numérateur et dénominateur)
        void reduce();

        //! accesseur numérateur
        int get_num() const;

        //! accesseur dénominateur
        int get_den() const;

        //! met la Fraction à 0 (0/1)
        void set_zero();

        //! modifie le numérateur
         /*!
          \param num le numérateur
        */
        void set_num(int num);

        //! modifie le dénominateur
         /*!
          \param num le dénominateur
        */
        void set_den(int den);

        //! Surcharge de l'opérateur d'incrémentation par une autre Fraction
        Fraction& operator+=(Fraction const& Fraction2);

        //! Surcharge de l'opérateur de décrémentation par une autre Fraction
        Fraction& operator-=(Fraction const& Fraction2);

        //! Surcharge de l'opérateur de multiplication par une autre Fraction
        Fraction& operator*=(Fraction const& Fraction2);

        //! Surcharge de l'opérateur de division par une autre Fraction
        Fraction& operator/=(Fraction const& Fraction2);

        //! destructeur
        virtual ~Fraction();

    protected:

    private:
        //! Gère le signe de la fraction
        /*! Met le signe - au numérateur de la fraction par défaut si le dénominateur est négatif.
         *  Rends la fraction positive si le numérateur et le dénominateur sont négatifs.
         */
        void minusHandler();

        //! initialisation du numérateur et du dénominateur
        int _num, _den;
};

//! Surcharge de l'opérateur d'affichage pour afficher l'objet "Fraction"
std::ostream& operator<<(std::ostream& out, Fraction const& fract);

//! Surcharge de l'opérateur d'addition
Fraction operator+(Fraction const& a, Fraction const& b);

//! Surcharge de l'opérateur de soustraction
Fraction operator-(Fraction const& a, Fraction const& b);

//! Surcharge de l'opérateur de multiplication
Fraction operator*(Fraction const& a, Fraction const& b);

//! Surcharge de l'opérateur de division
Fraction operator/(Fraction const& a, Fraction const& b);

#endif // FRACTION_H

#include "include/FractVector.h"

FractVector::FractVector(int nb_col):FractMatrix(1, nb_col)
{

}

FractVector::~FractVector()
{
    //dtor
}

Fraction& FractVector::operator[](int i){
    if (i >= _nb_columns)
        throw out_of_range("Vector subscript out of bounds");
    return *_matrix[0][i];
}

Fraction FractVector::operator[](int i) const{
    if (i >= _nb_columns)
        throw out_of_range("Vector subscript out of bounds");
    return *_matrix[0][i];
}


Fraction scalarProduct(FractVector const& A, FractVector const& B){
    if (A.get_nb_rows()!=1 || B.get_nb_rows()!=1 || A.get_nb_columns()!=B.get_nb_columns())
        throw std::length_error( "Error: 'A' and 'B' must be row vectors and have the same lenght." );
    Fraction * result = new Fraction(0);
    for (int i=0;i<A.get_nb_columns();++i){
            *result += A[i]*B[i];
        }
    return *result;
}


FractVector operator^(FractVector const& A, FractVector const& B){
    if (A.get_nb_rows()!=1 || B.get_nb_rows()!=1)
        throw std::length_error( "Error: 'A' and 'B' must be row vectors." );
    if (A.get_nb_columns()!=3 || B.get_nb_columns()!=3)
        throw std::length_error( "Error: 'A' and 'B' must have a length of 3 for vectorial product." );
    FractVector vectProd(3);
    vectProd[0] = A[1]*B[2]-A[2]*B[1];
    vectProd[1] = A[2]*B[0]-A[0]*B[2];
    vectProd[2] = A[0]*B[1]-A[1]*B[0];
    return vectProd;
}



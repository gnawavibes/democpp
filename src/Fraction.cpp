#include "include/Fraction.h"
#include <iostream>
#include <cmath>

using namespace std;

Fraction::Fraction(int num, int den){
    _num=num;
    _den=den;
}

Fraction::Fraction(int num){
    _num=num;
    _den=1;
}

Fraction::Fraction(){
    _num=0;
    _den=1;
}

Fraction::Fraction(Fraction const& tocopy){
    _num=tocopy.get_num();
    _den=tocopy.get_den();
}

Fraction::~Fraction(){
    //dtor
}


void Fraction::display(ostream &out) const{
    if (_num != 0){
        if(_den!=1)
            out << _num << "/" << _den;
        else
            out << _num;
    }
    else
        out << 0;
}

ostream& operator<<(ostream& out, Fraction const& fract){
    fract.display(out) ;
    return out;
}

int Fraction::get_num() const{
    return _num;
}

int Fraction::get_den() const{
    return _den;
}

void Fraction::set_zero(){
    _num = 0;
    _den = 1;
}

void Fraction::set_num(int num){
    _num = num;
    this->reduce();
}

void Fraction::set_den(int den){
    _den = den;
    this->reduce();
}

void Fraction::reduce(){
    for(int i = abs(_num*_den); i>1; i--){
        if(_num%i==0 && _den%i==0){
            _num/=i;
            _den/=i;
        }
    }
    this->minusHandler();
}

void Fraction::minusHandler(){
    if(_den<0){
        _num*=-1;
        _den*=-1;
    }
}


Fraction& Fraction::operator+=(const Fraction &Fraction2){
    // 1 : mise au meme dénominateur
    int num2 = Fraction2._num * _den;
    _den *= Fraction2._den;
    _num *= Fraction2._den;

    //2 : addition des numérateurs
    _num += num2;
    this->reduce();
    return *this;
}

Fraction& Fraction::operator-=(const Fraction &Fraction2){
    // 1 : mise au meme dénominateur
    int num2 = Fraction2._num * _den;
    _den *= Fraction2._den;
    _num *= Fraction2._den;

    //2 : addition des numérateurs
    _num -= num2;

    //3: clean fraction
    this->minusHandler();
    this->reduce();

    return *this;
}

Fraction& Fraction::operator*=(const Fraction &Fraction2){
    // 1 : multiplication des num et den entre eux
    _den *= Fraction2._den;
    _num *= Fraction2._num;

    //2: clean fraction
    this->minusHandler();
    this->reduce();

    return *this;
}

Fraction& Fraction::operator/=(const Fraction &Fraction2){
    // 1 : multiplication des num et den entre eux
    _den *= Fraction2._num;
    _num *= Fraction2._den;

    //2: clean fraction
    this->minusHandler();
    this->reduce();

    return *this;
}


Fraction operator+(Fraction const& a, Fraction const& b){
    Fraction copie(a);
    copie += b;
    return copie;
}

Fraction operator-(Fraction const& a, Fraction const& b){
    Fraction copie(a);
    copie -= b;
    return copie;
}

Fraction operator*(Fraction const& a, Fraction const& b){
    Fraction copie(a);
    copie *= b;
    return copie;
}

Fraction operator/(Fraction const& a, Fraction const& b){
    Fraction copie(a);
    copie /= b;
    return copie;
}

#include "include/FractMatrix.h"
#include "include/Fraction.h"

#include <stdlib.h>
#include <time.h>
#include <stdexcept>

FractMatrix::FractMatrix(int nb_rows, int nb_columns){
    _nb_rows = nb_rows;
    _nb_columns = nb_columns;
    for (int i=0;i<_nb_rows;++i)
    {
        _matrix.push_back(vector<Fraction *>(_nb_columns));
        for (int j=0;j<_nb_columns;++j){
            _matrix[i][j]= new Fraction();
        }
    }
}

FractMatrix::FractMatrix(FractMatrix const& A){
    _nb_rows = A.get_nb_rows();
    _nb_columns = A.get_nb_columns();
    for (int i=0;i<_nb_rows;++i)
    {
        _matrix.push_back(vector<Fraction *>(_nb_columns));
        for (int j=0;j<_nb_columns;++j){
            _matrix[i][j]= new Fraction(A(i,j));
        }
    }
}

FractMatrix::~FractMatrix(){
    //dtor
}



void FractMatrix::display(ostream &out) const{
    out << endl << "[";
    for (int i=0;i<_nb_rows;++i)
    {
        if (i!=0)
            out << " ";
        out << "[";
        for (int j=0;j<_nb_columns;++j)
        {
            out << " " << *_matrix[i][j];
            if (j!=_nb_columns-1)
                out << ",";
        }
        out << "]";
        if (i!=_nb_rows-1)
            out << endl;
    }
    out << "]" << endl;
}

ostream& operator<<(ostream& out, FractMatrix const& matrix){
    matrix.display(out) ;
    return out;
}



int FractMatrix::get_nb_rows() const{
    return _nb_rows;
}

int FractMatrix::get_nb_columns() const{
    return _nb_columns;
}

vector<vector<Fraction *> > FractMatrix::get_matrix() const{
    return _matrix;
}



void FractMatrix::set_nb_rows(int nb_rows){
    _nb_rows = nb_rows;
}

void FractMatrix::set_nb_columns(int nb_columns){
    _nb_columns = nb_columns;
}



Fraction& FractMatrix::operator()(int i, int j){
    if (i >= _nb_rows || j >= _nb_columns)
        throw out_of_range("Matrix subscript out of bounds");
    return *_matrix[i][j];
}

Fraction FractMatrix::operator()(int i, int j) const{
    if (i >= _nb_rows || j >= _nb_columns)
        throw out_of_range("Matrix subscript out of bounds");
    return *_matrix[i][j];
}

int GetRandomInt(int maxborns){
    // srand((unsigned) time(NULL));
    return (rand() % maxborns)-(maxborns/2);
}

void FractMatrix::FillWithZeros(){
    for (int i=0;i<_nb_rows;++i){
        for (int j=0;j<_nb_columns;++j){
            _matrix[i][j]= new Fraction(0);
        }
    }
}

void FractMatrix::FillRandomly(){
    int a, b;
    for (int i=0;i<_nb_rows;++i){
        for (int j=0;j<_nb_columns;++j){
            a = GetRandomInt(20);
            b = GetRandomInt(20);
            if (b==0){ // den can not be null!
                b=1;
            }
            _matrix[i][j]= new Fraction(a,b);
            _matrix[i][j]->reduce();
        }
    }
}



FractMatrix& FractMatrix::operator+=(FractMatrix const& FractMatrix2){
    if (_nb_rows!=FractMatrix2._nb_rows || _nb_columns!=FractMatrix2._nb_columns)
        throw std::length_error( "Error: The matrices must have the same sizes!" );
    for (int i=0;i<_nb_rows;++i){
        for (int j=0;j<_nb_columns;++j){
            *_matrix[i][j] += FractMatrix2(i,j);
            _matrix[i][j]->reduce();
        }
    }
    return *this;
}

FractMatrix& FractMatrix::operator-=(FractMatrix const& FractMatrix2){
    if (_nb_rows!=FractMatrix2._nb_rows || _nb_columns!=FractMatrix2._nb_columns)
        throw std::length_error( "Error: The matrices must have the same sizes!" );
    for (int i=0;i<_nb_rows;++i)
    {
        for (int j=0;j<_nb_columns;++j)
        {
            *_matrix[i][j] -= *FractMatrix2._matrix[i][j];
            _matrix[i][j]->reduce();
        }
    }
    return *this;
}

FractMatrix FractMatrix::T(){
    FractMatrix Transpose(_nb_columns, _nb_rows);
    for (int i=0;i<_nb_rows;++i){
        for (int j=0;j<_nb_columns;++j){
            Transpose(j,i) = *_matrix[i][j];
        }
    }
    return Transpose;
}

FractMatrix operator+(FractMatrix const& a, FractMatrix const& b){
    FractMatrix copie = a;
    copie += b;
    return copie;
}

FractMatrix operator-(FractMatrix const& a, FractMatrix const& b){
    FractMatrix copie = a;
    copie -= b;
    return copie;
}

FractMatrix operator*(FractMatrix const& a, FractMatrix const& b){
    if (a.get_nb_columns()!=b.get_nb_rows())
        throw std::length_error( "Error: Matrices sizes issue: if A*B, 'A' rows length must be equal to 'B' columns length " );
    FractMatrix multmatrix(a.get_nb_rows(), b.get_nb_columns());
    Fraction *pfractProduct(0);
    for (int i=0;i<multmatrix.get_nb_rows();++i){
        for (int j=0;j<multmatrix.get_nb_columns();++j){
            pfractProduct = &multmatrix(i,j);
            for (int k=0; k<a.get_nb_columns(); ++k){
                *pfractProduct += (a(i,k)*b(k,j));
            }
        }
    }
    return multmatrix;
}


/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "kbusyindicatorwidget.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_propos;
    QWidget *centralwidget;
    QTabWidget *MainWidgetTab;
    QWidget *demoFractionTab;
    QWidget *layoutWidget;
    QVBoxLayout *Fraction_form;
    QLabel *label;
    QTextBrowser *textBrowser;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_8;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_9;
    QSpinBox *num_A;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_10;
    QSpinBox *den_A;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_11;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_12;
    QSpinBox *num_B;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_13;
    QSpinBox *den_B;
    QVBoxLayout *operator_form;
    QLabel *label_14;
    QHBoxLayout *horizontalLayout_11;
    QRadioButton *Add_Fract_Button;
    QRadioButton *Sous_Fract_Button;
    QRadioButton *Mult_Fract_Button;
    QRadioButton *Div_Fract_Button;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *runFractionButton;
    QSpacerItem *horizontalSpacer_2;
    QTextBrowser *Fraction_results_TextBrowser;
    QWidget *demoMatrixTab;
    QWidget *layoutWidget1;
    QVBoxLayout *Matrix_Form;
    QLabel *label_2;
    QTextBrowser *textBrowser_2;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *Matrix_interract_area;
    QVBoxLayout *Matrix_input_Form;
    QHBoxLayout *Matrix_define_form;
    QVBoxLayout *Matrix_A_define_form;
    QLabel *label_15;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_17;
    QSpinBox *Nb_lignes_A;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_18;
    QSpinBox *Nb_colones_A;
    QVBoxLayout *Matrix_B_define_form;
    QLabel *label_16;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_20;
    QSpinBox *Nb_lignes_B;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_21;
    QSpinBox *Nb_colones_B;
    QVBoxLayout *verticalLayout_6;
    QLabel *label_22;
    QVBoxLayout *Select_Mat_operation;
    QRadioButton *Add_Mat_Button;
    QRadioButton *Sous_Mat_Button;
    QRadioButton *Mult_Mat_Button;
    QPushButton *runMatrixButton;
    QFrame *line;
    QTextBrowser *Matrix_results_TextBrowser;
    QWidget *demoVectorTab;
    QWidget *layoutWidget_2;
    QVBoxLayout *Vector_Form;
    QLabel *label_3;
    QTextBrowser *textBrowser_3;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *Vector_interract_area;
    QVBoxLayout *Vector_input_Form;
    QHBoxLayout *Vector_define_form;
    QVBoxLayout *Vector_A_define_form;
    QLabel *label_19;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_23;
    QSpinBox *Taille_Vect_A;
    QVBoxLayout *Vector_B_define_form;
    QLabel *label_25;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_26;
    QSpinBox *Taille_Vect_B;
    QVBoxLayout *Vector_select_operator_form;
    QLabel *label_28;
    QVBoxLayout *Select_Vec_operation;
    QRadioButton *Add_Vec_Button;
    QRadioButton *Sous_Vec_Button;
    QRadioButton *Mult_Vec_Button;
    QRadioButton *Scal_Vec_Button;
    QRadioButton *Vect_Vec_Button;
    QPushButton *runVectorButton;
    QFrame *line_2;
    QTextBrowser *Vector_results_TextBrowser;
    QWidget *demoThreadingTab;
    QLabel *label_4;
    QTextBrowser *textBrowser_4;
    QLabel *label_6;
    QPushButton *displayThreadsMatrixButton;
    QPushButton *displayThreadsVectorButton;
    QLabel *label_7;
    QDoubleSpinBox *timeOutMatrixSpinBox;
    QDoubleSpinBox *timeOutVectorSpinBox;
    QFrame *line_4;
    QLabel *label_32;
    QLabel *label_33;
    QPushButton *getMaxThreadsButton;
    QLabel *label_62;
    QTextBrowser *maxThreadsTextBrowser;
    QWidget *layoutWidget2;
    QHBoxLayout *horizontalLayout_15;
    QPushButton *runThreadsMatrixButton;
    QWidget *onGoingTestMatrix;
    QHBoxLayout *pp;
    KBusyIndicatorWidget *kbusyindicatorwidget;
    QLabel *label_5;
    QWidget *layoutWidget3;
    QHBoxLayout *horizontalLayout_17;
    QPushButton *runThreadsVectorButton;
    QWidget *onGoingTestVector;
    QHBoxLayout *oo;
    KBusyIndicatorWidget *kbusyindicatorwidget_2;
    QLabel *label_24;
    QMenuBar *menubar;
    QMenu *menuMenu;
    QStatusBar *statusbar;
    QToolBar *toolBar;
    QButtonGroup *selectFractionOperatorButtonGroup;
    QButtonGroup *selectMatrixOperatorButtonGroup;
    QButtonGroup *selectVectorOperatorButtonGroup;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(800, 600);
        QSizePolicy sizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        action_propos = new QAction(MainWindow);
        action_propos->setObjectName(QString::fromUtf8("action_propos"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralwidget->sizePolicy().hasHeightForWidth());
        centralwidget->setSizePolicy(sizePolicy1);
        MainWidgetTab = new QTabWidget(centralwidget);
        MainWidgetTab->setObjectName(QString::fromUtf8("MainWidgetTab"));
        MainWidgetTab->setEnabled(true);
        MainWidgetTab->setGeometry(QRect(10, 0, 781, 551));
        sizePolicy1.setHeightForWidth(MainWidgetTab->sizePolicy().hasHeightForWidth());
        MainWidgetTab->setSizePolicy(sizePolicy1);
        MainWidgetTab->setMovable(true);
        demoFractionTab = new QWidget();
        demoFractionTab->setObjectName(QString::fromUtf8("demoFractionTab"));
        QSizePolicy sizePolicy2(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(demoFractionTab->sizePolicy().hasHeightForWidth());
        demoFractionTab->setSizePolicy(sizePolicy2);
        demoFractionTab->setAutoFillBackground(false);
        layoutWidget = new QWidget(demoFractionTab);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 10, 751, 481));
        Fraction_form = new QVBoxLayout(layoutWidget);
        Fraction_form->setSpacing(10);
        Fraction_form->setObjectName(QString::fromUtf8("Fraction_form"));
        Fraction_form->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        sizePolicy1.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy1);
        QFont font;
        font.setFamily(QString::fromUtf8("DejaVu Sans"));
        font.setPointSize(14);
        font.setBold(true);
        font.setUnderline(true);
        font.setWeight(75);
        label->setFont(font);
        label->setTextFormat(Qt::RichText);

        Fraction_form->addWidget(label);

        textBrowser = new QTextBrowser(layoutWidget);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        QSizePolicy sizePolicy3(QSizePolicy::Ignored, QSizePolicy::Maximum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(textBrowser->sizePolicy().hasHeightForWidth());
        textBrowser->setSizePolicy(sizePolicy3);
        textBrowser->setSizeIncrement(QSize(0, 0));
        textBrowser->setBaseSize(QSize(0, 0));

        Fraction_form->addWidget(textBrowser);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(30);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(-1, -1, -1, 0);
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setUnderline(true);
        font1.setWeight(75);
        label_8->setFont(font1);
        label_8->setMidLineWidth(-4);
        label_8->setTextFormat(Qt::MarkdownText);

        verticalLayout_3->addWidget(label_8);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_9 = new QLabel(layoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout_7->addWidget(label_9);

        num_A = new QSpinBox(layoutWidget);
        num_A->setObjectName(QString::fromUtf8("num_A"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(num_A->sizePolicy().hasHeightForWidth());
        num_A->setSizePolicy(sizePolicy4);
        num_A->setMinimum(-99);
        num_A->setValue(0);

        horizontalLayout_7->addWidget(num_A);


        verticalLayout_3->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_10 = new QLabel(layoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));

        horizontalLayout_8->addWidget(label_10);

        den_A = new QSpinBox(layoutWidget);
        den_A->setObjectName(QString::fromUtf8("den_A"));
        den_A->setFrame(true);
        den_A->setMinimum(-99);
        den_A->setValue(0);

        horizontalLayout_8->addWidget(den_A);


        verticalLayout_3->addLayout(horizontalLayout_8);


        horizontalLayout_6->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(0);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_11 = new QLabel(layoutWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setFont(font1);
        label_11->setMidLineWidth(-4);
        label_11->setTextFormat(Qt::MarkdownText);

        verticalLayout_4->addWidget(label_11);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_12 = new QLabel(layoutWidget);
        label_12->setObjectName(QString::fromUtf8("label_12"));

        horizontalLayout_9->addWidget(label_12);

        num_B = new QSpinBox(layoutWidget);
        num_B->setObjectName(QString::fromUtf8("num_B"));
        num_B->setMinimum(-99);
        num_B->setValue(0);

        horizontalLayout_9->addWidget(num_B);


        verticalLayout_4->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_13 = new QLabel(layoutWidget);
        label_13->setObjectName(QString::fromUtf8("label_13"));

        horizontalLayout_10->addWidget(label_13);

        den_B = new QSpinBox(layoutWidget);
        den_B->setObjectName(QString::fromUtf8("den_B"));
        den_B->setMinimum(-99);

        horizontalLayout_10->addWidget(den_B);


        verticalLayout_4->addLayout(horizontalLayout_10);


        horizontalLayout_6->addLayout(verticalLayout_4);


        Fraction_form->addLayout(horizontalLayout_6);

        operator_form = new QVBoxLayout();
        operator_form->setObjectName(QString::fromUtf8("operator_form"));
        label_14 = new QLabel(layoutWidget);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setFont(font1);
        label_14->setMidLineWidth(-4);
        label_14->setTextFormat(Qt::MarkdownText);

        operator_form->addWidget(label_14);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        Add_Fract_Button = new QRadioButton(layoutWidget);
        selectFractionOperatorButtonGroup = new QButtonGroup(MainWindow);
        selectFractionOperatorButtonGroup->setObjectName(QString::fromUtf8("selectFractionOperatorButtonGroup"));
        selectFractionOperatorButtonGroup->addButton(Add_Fract_Button);
        Add_Fract_Button->setObjectName(QString::fromUtf8("Add_Fract_Button"));
        Add_Fract_Button->setChecked(true);

        horizontalLayout_11->addWidget(Add_Fract_Button);

        Sous_Fract_Button = new QRadioButton(layoutWidget);
        selectFractionOperatorButtonGroup->addButton(Sous_Fract_Button);
        Sous_Fract_Button->setObjectName(QString::fromUtf8("Sous_Fract_Button"));

        horizontalLayout_11->addWidget(Sous_Fract_Button);

        Mult_Fract_Button = new QRadioButton(layoutWidget);
        selectFractionOperatorButtonGroup->addButton(Mult_Fract_Button);
        Mult_Fract_Button->setObjectName(QString::fromUtf8("Mult_Fract_Button"));

        horizontalLayout_11->addWidget(Mult_Fract_Button);

        Div_Fract_Button = new QRadioButton(layoutWidget);
        selectFractionOperatorButtonGroup->addButton(Div_Fract_Button);
        Div_Fract_Button->setObjectName(QString::fromUtf8("Div_Fract_Button"));

        horizontalLayout_11->addWidget(Div_Fract_Button);


        operator_form->addLayout(horizontalLayout_11);


        Fraction_form->addLayout(operator_form);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        runFractionButton = new QPushButton(layoutWidget);
        runFractionButton->setObjectName(QString::fromUtf8("runFractionButton"));
        QSizePolicy sizePolicy5(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(runFractionButton->sizePolicy().hasHeightForWidth());
        runFractionButton->setSizePolicy(sizePolicy5);
        runFractionButton->setCheckable(false);
        runFractionButton->setChecked(false);

        horizontalLayout->addWidget(runFractionButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        Fraction_form->addLayout(horizontalLayout);

        Fraction_results_TextBrowser = new QTextBrowser(layoutWidget);
        Fraction_results_TextBrowser->setObjectName(QString::fromUtf8("Fraction_results_TextBrowser"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Monospace"));
        Fraction_results_TextBrowser->setFont(font2);

        Fraction_form->addWidget(Fraction_results_TextBrowser);

        Fraction_form->setStretch(0, 1);
        Fraction_form->setStretch(1, 7);
        Fraction_form->setStretch(2, 3);
        Fraction_form->setStretch(3, 2);
        Fraction_form->setStretch(4, 1);
        Fraction_form->setStretch(5, 6);
        MainWidgetTab->addTab(demoFractionTab, QString());
        demoMatrixTab = new QWidget();
        demoMatrixTab->setObjectName(QString::fromUtf8("demoMatrixTab"));
        layoutWidget1 = new QWidget(demoMatrixTab);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(10, 10, 751, 481));
        Matrix_Form = new QVBoxLayout(layoutWidget1);
        Matrix_Form->setSpacing(10);
        Matrix_Form->setObjectName(QString::fromUtf8("Matrix_Form"));
        Matrix_Form->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(layoutWidget1);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QSizePolicy sizePolicy6(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy6);
        label_2->setFont(font);
        label_2->setTextFormat(Qt::RichText);

        Matrix_Form->addWidget(label_2);

        textBrowser_2 = new QTextBrowser(layoutWidget1);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));
        sizePolicy3.setHeightForWidth(textBrowser_2->sizePolicy().hasHeightForWidth());
        textBrowser_2->setSizePolicy(sizePolicy3);
        textBrowser_2->setSizeIncrement(QSize(0, 0));
        textBrowser_2->setBaseSize(QSize(0, 0));

        Matrix_Form->addWidget(textBrowser_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        Matrix_Form->addItem(verticalSpacer);

        Matrix_interract_area = new QHBoxLayout();
        Matrix_interract_area->setSpacing(15);
        Matrix_interract_area->setObjectName(QString::fromUtf8("Matrix_interract_area"));
        Matrix_input_Form = new QVBoxLayout();
        Matrix_input_Form->setSpacing(30);
        Matrix_input_Form->setObjectName(QString::fromUtf8("Matrix_input_Form"));
        Matrix_define_form = new QHBoxLayout();
        Matrix_define_form->setObjectName(QString::fromUtf8("Matrix_define_form"));
        Matrix_A_define_form = new QVBoxLayout();
        Matrix_A_define_form->setSpacing(3);
        Matrix_A_define_form->setObjectName(QString::fromUtf8("Matrix_A_define_form"));
        label_15 = new QLabel(layoutWidget1);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setFont(font1);
        label_15->setMidLineWidth(-4);
        label_15->setTextFormat(Qt::MarkdownText);

        Matrix_A_define_form->addWidget(label_15);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_17 = new QLabel(layoutWidget1);
        label_17->setObjectName(QString::fromUtf8("label_17"));

        horizontalLayout_3->addWidget(label_17);

        Nb_lignes_A = new QSpinBox(layoutWidget1);
        Nb_lignes_A->setObjectName(QString::fromUtf8("Nb_lignes_A"));
        sizePolicy4.setHeightForWidth(Nb_lignes_A->sizePolicy().hasHeightForWidth());
        Nb_lignes_A->setSizePolicy(sizePolicy4);
        Nb_lignes_A->setMinimum(1);
        Nb_lignes_A->setValue(1);

        horizontalLayout_3->addWidget(Nb_lignes_A);


        Matrix_A_define_form->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_18 = new QLabel(layoutWidget1);
        label_18->setObjectName(QString::fromUtf8("label_18"));

        horizontalLayout_2->addWidget(label_18);

        Nb_colones_A = new QSpinBox(layoutWidget1);
        Nb_colones_A->setObjectName(QString::fromUtf8("Nb_colones_A"));
        sizePolicy4.setHeightForWidth(Nb_colones_A->sizePolicy().hasHeightForWidth());
        Nb_colones_A->setSizePolicy(sizePolicy4);
        Nb_colones_A->setMinimum(1);
        Nb_colones_A->setValue(1);

        horizontalLayout_2->addWidget(Nb_colones_A);


        Matrix_A_define_form->addLayout(horizontalLayout_2);


        Matrix_define_form->addLayout(Matrix_A_define_form);

        Matrix_B_define_form = new QVBoxLayout();
        Matrix_B_define_form->setSpacing(3);
        Matrix_B_define_form->setObjectName(QString::fromUtf8("Matrix_B_define_form"));
        label_16 = new QLabel(layoutWidget1);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setFont(font1);
        label_16->setMidLineWidth(-4);
        label_16->setTextFormat(Qt::MarkdownText);

        Matrix_B_define_form->addWidget(label_16);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_20 = new QLabel(layoutWidget1);
        label_20->setObjectName(QString::fromUtf8("label_20"));

        horizontalLayout_5->addWidget(label_20);

        Nb_lignes_B = new QSpinBox(layoutWidget1);
        Nb_lignes_B->setObjectName(QString::fromUtf8("Nb_lignes_B"));
        sizePolicy4.setHeightForWidth(Nb_lignes_B->sizePolicy().hasHeightForWidth());
        Nb_lignes_B->setSizePolicy(sizePolicy4);
        Nb_lignes_B->setMinimum(1);
        Nb_lignes_B->setValue(1);

        horizontalLayout_5->addWidget(Nb_lignes_B);


        Matrix_B_define_form->addLayout(horizontalLayout_5);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_21 = new QLabel(layoutWidget1);
        label_21->setObjectName(QString::fromUtf8("label_21"));

        horizontalLayout_12->addWidget(label_21);

        Nb_colones_B = new QSpinBox(layoutWidget1);
        Nb_colones_B->setObjectName(QString::fromUtf8("Nb_colones_B"));
        sizePolicy4.setHeightForWidth(Nb_colones_B->sizePolicy().hasHeightForWidth());
        Nb_colones_B->setSizePolicy(sizePolicy4);
        Nb_colones_B->setMinimum(1);
        Nb_colones_B->setValue(1);

        horizontalLayout_12->addWidget(Nb_colones_B);


        Matrix_B_define_form->addLayout(horizontalLayout_12);


        Matrix_define_form->addLayout(Matrix_B_define_form);


        Matrix_input_Form->addLayout(Matrix_define_form);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(5);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label_22 = new QLabel(layoutWidget1);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setFont(font1);
        label_22->setMidLineWidth(-4);
        label_22->setTextFormat(Qt::MarkdownText);

        verticalLayout_6->addWidget(label_22);

        Select_Mat_operation = new QVBoxLayout();
        Select_Mat_operation->setSpacing(4);
        Select_Mat_operation->setObjectName(QString::fromUtf8("Select_Mat_operation"));
        Add_Mat_Button = new QRadioButton(layoutWidget1);
        selectMatrixOperatorButtonGroup = new QButtonGroup(MainWindow);
        selectMatrixOperatorButtonGroup->setObjectName(QString::fromUtf8("selectMatrixOperatorButtonGroup"));
        selectMatrixOperatorButtonGroup->addButton(Add_Mat_Button);
        Add_Mat_Button->setObjectName(QString::fromUtf8("Add_Mat_Button"));
        Add_Mat_Button->setChecked(true);

        Select_Mat_operation->addWidget(Add_Mat_Button);

        Sous_Mat_Button = new QRadioButton(layoutWidget1);
        selectMatrixOperatorButtonGroup->addButton(Sous_Mat_Button);
        Sous_Mat_Button->setObjectName(QString::fromUtf8("Sous_Mat_Button"));

        Select_Mat_operation->addWidget(Sous_Mat_Button);

        Mult_Mat_Button = new QRadioButton(layoutWidget1);
        selectMatrixOperatorButtonGroup->addButton(Mult_Mat_Button);
        Mult_Mat_Button->setObjectName(QString::fromUtf8("Mult_Mat_Button"));

        Select_Mat_operation->addWidget(Mult_Mat_Button);


        verticalLayout_6->addLayout(Select_Mat_operation);

        verticalLayout_6->setStretch(0, 1);
        verticalLayout_6->setStretch(1, 4);

        Matrix_input_Form->addLayout(verticalLayout_6);

        runMatrixButton = new QPushButton(layoutWidget1);
        runMatrixButton->setObjectName(QString::fromUtf8("runMatrixButton"));
        sizePolicy5.setHeightForWidth(runMatrixButton->sizePolicy().hasHeightForWidth());
        runMatrixButton->setSizePolicy(sizePolicy5);
        runMatrixButton->setCheckable(false);
        runMatrixButton->setChecked(false);

        Matrix_input_Form->addWidget(runMatrixButton);

        Matrix_input_Form->setStretch(0, 4);
        Matrix_input_Form->setStretch(1, 4);
        Matrix_input_Form->setStretch(2, 1);

        Matrix_interract_area->addLayout(Matrix_input_Form);

        line = new QFrame(layoutWidget1);
        line->setObjectName(QString::fromUtf8("line"));
        line->setStyleSheet(QString::fromUtf8("background-color:rgb(0,0,0)"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        Matrix_interract_area->addWidget(line);

        Matrix_results_TextBrowser = new QTextBrowser(layoutWidget1);
        Matrix_results_TextBrowser->setObjectName(QString::fromUtf8("Matrix_results_TextBrowser"));
        Matrix_results_TextBrowser->setFont(font2);

        Matrix_interract_area->addWidget(Matrix_results_TextBrowser);


        Matrix_Form->addLayout(Matrix_interract_area);

        Matrix_Form->setStretch(0, 1);
        Matrix_Form->setStretch(1, 3);
        Matrix_Form->setStretch(2, 1);
        Matrix_Form->setStretch(3, 20);
        MainWidgetTab->addTab(demoMatrixTab, QString());
        demoVectorTab = new QWidget();
        demoVectorTab->setObjectName(QString::fromUtf8("demoVectorTab"));
        layoutWidget_2 = new QWidget(demoVectorTab);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(10, 10, 751, 481));
        Vector_Form = new QVBoxLayout(layoutWidget_2);
        Vector_Form->setSpacing(10);
        Vector_Form->setObjectName(QString::fromUtf8("Vector_Form"));
        Vector_Form->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(layoutWidget_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        sizePolicy6.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy6);
        label_3->setFont(font);
        label_3->setTextFormat(Qt::RichText);

        Vector_Form->addWidget(label_3);

        textBrowser_3 = new QTextBrowser(layoutWidget_2);
        textBrowser_3->setObjectName(QString::fromUtf8("textBrowser_3"));
        sizePolicy3.setHeightForWidth(textBrowser_3->sizePolicy().hasHeightForWidth());
        textBrowser_3->setSizePolicy(sizePolicy3);
        textBrowser_3->setSizeIncrement(QSize(0, 0));
        textBrowser_3->setBaseSize(QSize(0, 0));

        Vector_Form->addWidget(textBrowser_3);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        Vector_Form->addItem(verticalSpacer_2);

        Vector_interract_area = new QHBoxLayout();
        Vector_interract_area->setSpacing(15);
        Vector_interract_area->setObjectName(QString::fromUtf8("Vector_interract_area"));
        Vector_input_Form = new QVBoxLayout();
        Vector_input_Form->setSpacing(20);
        Vector_input_Form->setObjectName(QString::fromUtf8("Vector_input_Form"));
        Vector_define_form = new QHBoxLayout();
        Vector_define_form->setObjectName(QString::fromUtf8("Vector_define_form"));
        Vector_A_define_form = new QVBoxLayout();
        Vector_A_define_form->setSpacing(3);
        Vector_A_define_form->setObjectName(QString::fromUtf8("Vector_A_define_form"));
        label_19 = new QLabel(layoutWidget_2);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setFont(font1);
        label_19->setMidLineWidth(-4);
        label_19->setTextFormat(Qt::MarkdownText);

        Vector_A_define_form->addWidget(label_19);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_23 = new QLabel(layoutWidget_2);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        horizontalLayout_4->addWidget(label_23);

        Taille_Vect_A = new QSpinBox(layoutWidget_2);
        Taille_Vect_A->setObjectName(QString::fromUtf8("Taille_Vect_A"));
        sizePolicy4.setHeightForWidth(Taille_Vect_A->sizePolicy().hasHeightForWidth());
        Taille_Vect_A->setSizePolicy(sizePolicy4);
        Taille_Vect_A->setMinimum(1);
        Taille_Vect_A->setValue(1);

        horizontalLayout_4->addWidget(Taille_Vect_A);


        Vector_A_define_form->addLayout(horizontalLayout_4);


        Vector_define_form->addLayout(Vector_A_define_form);

        Vector_B_define_form = new QVBoxLayout();
        Vector_B_define_form->setSpacing(3);
        Vector_B_define_form->setObjectName(QString::fromUtf8("Vector_B_define_form"));
        label_25 = new QLabel(layoutWidget_2);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setFont(font1);
        label_25->setMidLineWidth(-4);
        label_25->setTextFormat(Qt::MarkdownText);

        Vector_B_define_form->addWidget(label_25);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_26 = new QLabel(layoutWidget_2);
        label_26->setObjectName(QString::fromUtf8("label_26"));

        horizontalLayout_14->addWidget(label_26);

        Taille_Vect_B = new QSpinBox(layoutWidget_2);
        Taille_Vect_B->setObjectName(QString::fromUtf8("Taille_Vect_B"));
        sizePolicy4.setHeightForWidth(Taille_Vect_B->sizePolicy().hasHeightForWidth());
        Taille_Vect_B->setSizePolicy(sizePolicy4);
        Taille_Vect_B->setMinimum(1);
        Taille_Vect_B->setValue(1);

        horizontalLayout_14->addWidget(Taille_Vect_B);


        Vector_B_define_form->addLayout(horizontalLayout_14);


        Vector_define_form->addLayout(Vector_B_define_form);


        Vector_input_Form->addLayout(Vector_define_form);

        Vector_select_operator_form = new QVBoxLayout();
        Vector_select_operator_form->setSpacing(0);
        Vector_select_operator_form->setObjectName(QString::fromUtf8("Vector_select_operator_form"));
        label_28 = new QLabel(layoutWidget_2);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setFont(font1);
        label_28->setMidLineWidth(-4);
        label_28->setTextFormat(Qt::MarkdownText);

        Vector_select_operator_form->addWidget(label_28);

        Select_Vec_operation = new QVBoxLayout();
        Select_Vec_operation->setSpacing(4);
        Select_Vec_operation->setObjectName(QString::fromUtf8("Select_Vec_operation"));
        Add_Vec_Button = new QRadioButton(layoutWidget_2);
        selectVectorOperatorButtonGroup = new QButtonGroup(MainWindow);
        selectVectorOperatorButtonGroup->setObjectName(QString::fromUtf8("selectVectorOperatorButtonGroup"));
        selectVectorOperatorButtonGroup->addButton(Add_Vec_Button);
        Add_Vec_Button->setObjectName(QString::fromUtf8("Add_Vec_Button"));
        Add_Vec_Button->setChecked(true);

        Select_Vec_operation->addWidget(Add_Vec_Button);

        Sous_Vec_Button = new QRadioButton(layoutWidget_2);
        selectVectorOperatorButtonGroup->addButton(Sous_Vec_Button);
        Sous_Vec_Button->setObjectName(QString::fromUtf8("Sous_Vec_Button"));

        Select_Vec_operation->addWidget(Sous_Vec_Button);

        Mult_Vec_Button = new QRadioButton(layoutWidget_2);
        selectVectorOperatorButtonGroup->addButton(Mult_Vec_Button);
        Mult_Vec_Button->setObjectName(QString::fromUtf8("Mult_Vec_Button"));

        Select_Vec_operation->addWidget(Mult_Vec_Button);

        Scal_Vec_Button = new QRadioButton(layoutWidget_2);
        selectVectorOperatorButtonGroup->addButton(Scal_Vec_Button);
        Scal_Vec_Button->setObjectName(QString::fromUtf8("Scal_Vec_Button"));

        Select_Vec_operation->addWidget(Scal_Vec_Button);

        Vect_Vec_Button = new QRadioButton(layoutWidget_2);
        selectVectorOperatorButtonGroup->addButton(Vect_Vec_Button);
        Vect_Vec_Button->setObjectName(QString::fromUtf8("Vect_Vec_Button"));

        Select_Vec_operation->addWidget(Vect_Vec_Button);


        Vector_select_operator_form->addLayout(Select_Vec_operation);

        Vector_select_operator_form->setStretch(0, 1);
        Vector_select_operator_form->setStretch(1, 4);

        Vector_input_Form->addLayout(Vector_select_operator_form);

        runVectorButton = new QPushButton(layoutWidget_2);
        runVectorButton->setObjectName(QString::fromUtf8("runVectorButton"));
        sizePolicy5.setHeightForWidth(runVectorButton->sizePolicy().hasHeightForWidth());
        runVectorButton->setSizePolicy(sizePolicy5);
        runVectorButton->setCheckable(false);
        runVectorButton->setChecked(false);

        Vector_input_Form->addWidget(runVectorButton);

        Vector_input_Form->setStretch(0, 2);
        Vector_input_Form->setStretch(1, 8);
        Vector_input_Form->setStretch(2, 1);

        Vector_interract_area->addLayout(Vector_input_Form);

        line_2 = new QFrame(layoutWidget_2);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setStyleSheet(QString::fromUtf8("background-color:rgb(0,0,0)"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        Vector_interract_area->addWidget(line_2);

        Vector_results_TextBrowser = new QTextBrowser(layoutWidget_2);
        Vector_results_TextBrowser->setObjectName(QString::fromUtf8("Vector_results_TextBrowser"));
        Vector_results_TextBrowser->setFont(font2);

        Vector_interract_area->addWidget(Vector_results_TextBrowser);


        Vector_Form->addLayout(Vector_interract_area);

        Vector_Form->setStretch(0, 1);
        Vector_Form->setStretch(1, 3);
        Vector_Form->setStretch(2, 1);
        Vector_Form->setStretch(3, 20);
        MainWidgetTab->addTab(demoVectorTab, QString());
        demoThreadingTab = new QWidget();
        demoThreadingTab->setObjectName(QString::fromUtf8("demoThreadingTab"));
        label_4 = new QLabel(demoThreadingTab);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 10, 749, 23));
        sizePolicy6.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy6);
        label_4->setFont(font);
        label_4->setTextFormat(Qt::RichText);
        textBrowser_4 = new QTextBrowser(demoThreadingTab);
        textBrowser_4->setObjectName(QString::fromUtf8("textBrowser_4"));
        textBrowser_4->setGeometry(QRect(10, 43, 749, 241));
        sizePolicy3.setHeightForWidth(textBrowser_4->sizePolicy().hasHeightForWidth());
        textBrowser_4->setSizePolicy(sizePolicy3);
        textBrowser_4->setSizeIncrement(QSize(0, 0));
        textBrowser_4->setBaseSize(QSize(0, 0));
        label_6 = new QLabel(demoThreadingTab);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(40, 390, 201, 16));
        displayThreadsMatrixButton = new QPushButton(demoThreadingTab);
        displayThreadsMatrixButton->setObjectName(QString::fromUtf8("displayThreadsMatrixButton"));
        displayThreadsMatrixButton->setEnabled(false);
        displayThreadsMatrixButton->setGeometry(QRect(40, 460, 131, 28));
        displayThreadsMatrixButton->setFlat(false);
        displayThreadsVectorButton = new QPushButton(demoThreadingTab);
        displayThreadsVectorButton->setObjectName(QString::fromUtf8("displayThreadsVectorButton"));
        displayThreadsVectorButton->setEnabled(false);
        displayThreadsVectorButton->setGeometry(QRect(430, 460, 131, 28));
        label_7 = new QLabel(demoThreadingTab);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(430, 390, 201, 16));
        timeOutMatrixSpinBox = new QDoubleSpinBox(demoThreadingTab);
        timeOutMatrixSpinBox->setObjectName(QString::fromUtf8("timeOutMatrixSpinBox"));
        timeOutMatrixSpinBox->setGeometry(QRect(210, 380, 122, 28));
        timeOutMatrixSpinBox->setDecimals(3);
        timeOutMatrixSpinBox->setValue(10.000000000000000);
        timeOutVectorSpinBox = new QDoubleSpinBox(demoThreadingTab);
        timeOutVectorSpinBox->setObjectName(QString::fromUtf8("timeOutVectorSpinBox"));
        timeOutVectorSpinBox->setGeometry(QRect(590, 380, 122, 28));
        timeOutVectorSpinBox->setDecimals(3);
        timeOutVectorSpinBox->setValue(3.000000000000000);
        line_4 = new QFrame(demoThreadingTab);
        line_4->setObjectName(QString::fromUtf8("line_4"));
        line_4->setGeometry(QRect(390, 330, 16, 171));
        sizePolicy6.setHeightForWidth(line_4->sizePolicy().hasHeightForWidth());
        line_4->setSizePolicy(sizePolicy6);
        line_4->setStyleSheet(QString::fromUtf8("background-color:rgb(0,0,0)"));
        line_4->setFrameShape(QFrame::VLine);
        line_4->setFrameShadow(QFrame::Sunken);
        label_32 = new QLabel(demoThreadingTab);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setGeometry(QRect(40, 340, 172, 20));
        label_32->setFont(font1);
        label_32->setMidLineWidth(-4);
        label_32->setTextFormat(Qt::MarkdownText);
        label_33 = new QLabel(demoThreadingTab);
        label_33->setObjectName(QString::fromUtf8("label_33"));
        label_33->setGeometry(QRect(430, 340, 172, 20));
        label_33->setFont(font1);
        label_33->setMidLineWidth(-4);
        label_33->setTextFormat(Qt::MarkdownText);
        getMaxThreadsButton = new QPushButton(demoThreadingTab);
        getMaxThreadsButton->setObjectName(QString::fromUtf8("getMaxThreadsButton"));
        getMaxThreadsButton->setGeometry(QRect(380, 290, 88, 28));
        label_62 = new QLabel(demoThreadingTab);
        label_62->setObjectName(QString::fromUtf8("label_62"));
        label_62->setGeometry(QRect(30, 300, 201, 16));
        maxThreadsTextBrowser = new QTextBrowser(demoThreadingTab);
        maxThreadsTextBrowser->setObjectName(QString::fromUtf8("maxThreadsTextBrowser"));
        maxThreadsTextBrowser->setGeometry(QRect(240, 290, 131, 31));
        layoutWidget2 = new QWidget(demoThreadingTab);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        layoutWidget2->setGeometry(QRect(40, 420, 291, 36));
        horizontalLayout_15 = new QHBoxLayout(layoutWidget2);
        horizontalLayout_15->setSpacing(36);
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        horizontalLayout_15->setContentsMargins(0, 0, 0, 0);
        runThreadsMatrixButton = new QPushButton(layoutWidget2);
        runThreadsMatrixButton->setObjectName(QString::fromUtf8("runThreadsMatrixButton"));

        horizontalLayout_15->addWidget(runThreadsMatrixButton);

        onGoingTestMatrix = new QWidget(layoutWidget2);
        onGoingTestMatrix->setObjectName(QString::fromUtf8("onGoingTestMatrix"));
        pp = new QHBoxLayout(onGoingTestMatrix);
        pp->setObjectName(QString::fromUtf8("pp"));
        kbusyindicatorwidget = new KBusyIndicatorWidget(onGoingTestMatrix);
        kbusyindicatorwidget->setObjectName(QString::fromUtf8("kbusyindicatorwidget"));
        kbusyindicatorwidget->setEnabled(true);

        pp->addWidget(kbusyindicatorwidget);

        label_5 = new QLabel(onGoingTestMatrix);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        pp->addWidget(label_5);


        horizontalLayout_15->addWidget(onGoingTestMatrix);

        layoutWidget3 = new QWidget(demoThreadingTab);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        layoutWidget3->setGeometry(QRect(430, 420, 291, 36));
        horizontalLayout_17 = new QHBoxLayout(layoutWidget3);
        horizontalLayout_17->setSpacing(30);
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        horizontalLayout_17->setContentsMargins(0, 0, 0, 0);
        runThreadsVectorButton = new QPushButton(layoutWidget3);
        runThreadsVectorButton->setObjectName(QString::fromUtf8("runThreadsVectorButton"));

        horizontalLayout_17->addWidget(runThreadsVectorButton);

        onGoingTestVector = new QWidget(layoutWidget3);
        onGoingTestVector->setObjectName(QString::fromUtf8("onGoingTestVector"));
        oo = new QHBoxLayout(onGoingTestVector);
        oo->setObjectName(QString::fromUtf8("oo"));
        kbusyindicatorwidget_2 = new KBusyIndicatorWidget(onGoingTestVector);
        kbusyindicatorwidget_2->setObjectName(QString::fromUtf8("kbusyindicatorwidget_2"));

        oo->addWidget(kbusyindicatorwidget_2);

        label_24 = new QLabel(onGoingTestVector);
        label_24->setObjectName(QString::fromUtf8("label_24"));

        oo->addWidget(label_24);


        horizontalLayout_17->addWidget(onGoingTestVector);

        MainWidgetTab->addTab(demoThreadingTab, QString());
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 800, 25));
        menuMenu = new QMenu(menubar);
        menuMenu->setObjectName(QString::fromUtf8("menuMenu"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QString::fromUtf8("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);

        menubar->addAction(menuMenu->menuAction());
        menuMenu->addAction(action_propos);

        retranslateUi(MainWindow);

        MainWidgetTab->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        action_propos->setText(QCoreApplication::translate("MainWindow", "\303\240 propos...", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Demo Fraction", nullptr));
        textBrowser->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Cantarell'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Description</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Calculatrice permettant de construire 2 fractions, de les r\303\251duires puis d'effectuer, au choix, l'une des 4 op\303\251rations arithm\303\251tiques de base:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- addition</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-righ"
                        "t:0px; -qt-block-indent:0; text-indent:0px;\">- soustraction</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- multiplication</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- division</p></body></html>", nullptr));
        label_8->setText(QCoreApplication::translate("MainWindow", "Fraction A:", nullptr));
        label_9->setText(QCoreApplication::translate("MainWindow", "num\303\251rateur:", nullptr));
        label_10->setText(QCoreApplication::translate("MainWindow", "d\303\251nominateur:", nullptr));
        label_11->setText(QCoreApplication::translate("MainWindow", "Fraction B:", nullptr));
        label_12->setText(QCoreApplication::translate("MainWindow", "num\303\251rateur:", nullptr));
        label_13->setText(QCoreApplication::translate("MainWindow", "d\303\251nominateur:", nullptr));
        label_14->setText(QCoreApplication::translate("MainWindow", "Choix de l'op\303\251ration:", nullptr));
        Add_Fract_Button->setText(QCoreApplication::translate("MainWindow", "Addition (+)", nullptr));
        Sous_Fract_Button->setText(QCoreApplication::translate("MainWindow", "Soustraction (-)", nullptr));
        Mult_Fract_Button->setText(QCoreApplication::translate("MainWindow", "Multiplication (*)", nullptr));
        Div_Fract_Button->setText(QCoreApplication::translate("MainWindow", "Division (/)", nullptr));
        runFractionButton->setText(QCoreApplication::translate("MainWindow", "Lancer le caclul", nullptr));
        Fraction_results_TextBrowser->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Monospace'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Cantarell';\"><br /></p></body></html>", nullptr));
        MainWidgetTab->setTabText(MainWidgetTab->indexOf(demoFractionTab), QCoreApplication::translate("MainWindow", "Demo Fraction", nullptr));
        label_2->setText(QCoreApplication::translate("MainWindow", "Demo Matrice", nullptr));
        textBrowser_2->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Cantarell'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Description</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">A partir de dimensions impos\303\251es par l'utilisateur, l'application va g\303\251n\303\251rer deux matrices de fractions al\303\251atoires. Puis, toujours selon le choix de l'utilisateur, ces deux matrices vont subir une transformation lin\303\251aire classique: addition, soustraction, multiplication.</p></body></html>", nullptr));
        label_15->setText(QCoreApplication::translate("MainWindow", "Matrice A:", nullptr));
        label_17->setText(QCoreApplication::translate("MainWindow", "Nb lignes:", nullptr));
        label_18->setText(QCoreApplication::translate("MainWindow", "Nb colonnes:", nullptr));
        label_16->setText(QCoreApplication::translate("MainWindow", "Matrice B:", nullptr));
        label_20->setText(QCoreApplication::translate("MainWindow", "Nb lignes:", nullptr));
        label_21->setText(QCoreApplication::translate("MainWindow", "Nb colonnes:", nullptr));
        label_22->setText(QCoreApplication::translate("MainWindow", "Choix de l'op\303\251ration:", nullptr));
        Add_Mat_Button->setText(QCoreApplication::translate("MainWindow", "Addition (+)", nullptr));
        Sous_Mat_Button->setText(QCoreApplication::translate("MainWindow", "Soustraction (-)", nullptr));
        Mult_Mat_Button->setText(QCoreApplication::translate("MainWindow", "Multiplication (*)", nullptr));
        runMatrixButton->setText(QCoreApplication::translate("MainWindow", "Lancer le caclul", nullptr));
        Matrix_results_TextBrowser->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Monospace'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Cantarell';\"><br /></p></body></html>", nullptr));
        MainWidgetTab->setTabText(MainWidgetTab->indexOf(demoMatrixTab), QCoreApplication::translate("MainWindow", "Demo Matrice", nullptr));
        label_3->setText(QCoreApplication::translate("MainWindow", "Demo Vecteurs", nullptr));
        textBrowser_3->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Cantarell'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Description</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">A partir des tailles impos\303\251es par l'utilisateur, l'application va g\303\251n\303\251rer deux vecteurs de fractions al\303\251atoires. </p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Si c'est math\303\251matiquement possible, l'algorithme r\303\251soud alors une op\303\251ration vectorielle chois"
                        "ie : addition, soustraction, multiplication ou calcul du produit scalaire.<br />Si les deux vecteurs sont de taille 3, le produit vectoriel est \303\251galement propos\303\251</p></body></html>", nullptr));
        label_19->setText(QCoreApplication::translate("MainWindow", "Vecteur A:", nullptr));
        label_23->setText(QCoreApplication::translate("MainWindow", "Taille", nullptr));
        label_25->setText(QCoreApplication::translate("MainWindow", "Vecteur B:", nullptr));
        label_26->setText(QCoreApplication::translate("MainWindow", "Taille", nullptr));
        label_28->setText(QCoreApplication::translate("MainWindow", "Choix de l'op\303\251ration:", nullptr));
        Add_Vec_Button->setText(QCoreApplication::translate("MainWindow", "Addition (+)", nullptr));
        Sous_Vec_Button->setText(QCoreApplication::translate("MainWindow", "Soustraction (-)", nullptr));
        Mult_Vec_Button->setText(QCoreApplication::translate("MainWindow", "Multiplication (*) de la transpos\303\251 de A par B", nullptr));
        Scal_Vec_Button->setText(QCoreApplication::translate("MainWindow", "Produit scalaire (.)", nullptr));
        Vect_Vec_Button->setText(QCoreApplication::translate("MainWindow", "Produit vectoriel (^)", nullptr));
        runVectorButton->setText(QCoreApplication::translate("MainWindow", "Lancer le caclul", nullptr));
        Vector_results_TextBrowser->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Monospace'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Cantarell';\"><br /></p></body></html>", nullptr));
        MainWidgetTab->setTabText(MainWidgetTab->indexOf(demoVectorTab), QCoreApplication::translate("MainWindow", "Demo Vecteur", nullptr));
        label_4->setText(QCoreApplication::translate("MainWindow", "Demo Threading", nullptr));
        textBrowser_4->setHtml(QCoreApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Cantarell'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-weight:600;\">Description</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Consid\303\251rons 2 scripts:</p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- matrice: pour une taille 'n' donn\303\251, le script g\303\251n\303\250re 2 matrices de fraction de taille n*n puis calcule leurs transpos\303\251es respectives, leur somme, leur diff\303\251rence et leur produit.</p>\n"
"<p"
                        " style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">- vecteur: pour une taille 'n' donn\303\251, le script g\303\251n\303\250re 2 vecteur de fraction de taille n, puis calcule leurs transpos\303\251es respectives, leur somme, leur diff\303\251rence et leur produit scalaire.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Dans chacun des cas, plus 'n' augmente, plus les scripts seront long \303\240 executer.</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Le but des deux"
                        " fonctions ci-dessous est de trouver le n maximum possible selon les cas (matrice ou vecteur), dans un temps imparti (timeout).</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Dans les deux cas, l'algorithme fonctionne de la mani\303\250re suivante: la fonction  g\303\251n\303\250re it\303\251rativement un thread par valeur de 'n' en commen\303\247ant par n=1. La fonction termine lorsque la dur\303\251e d'execution d'un thread d\303\251passe le timeout fix\303\251.</p></body></html>", nullptr));
        label_6->setText(QCoreApplication::translate("MainWindow", "timeout pour Matrices (s):", nullptr));
        displayThreadsMatrixButton->setText(QCoreApplication::translate("MainWindow", "Afficher les r\303\251sultats", nullptr));
        displayThreadsVectorButton->setText(QCoreApplication::translate("MainWindow", "Afficher les r\303\251sultats", nullptr));
        label_7->setText(QCoreApplication::translate("MainWindow", "timeout pour Vecteurs (s):", nullptr));
        label_32->setText(QCoreApplication::translate("MainWindow", "test matrice", nullptr));
        label_33->setText(QCoreApplication::translate("MainWindow", "test vecteur", nullptr));
        getMaxThreadsButton->setText(QCoreApplication::translate("MainWindow", "Obtenir", nullptr));
        label_62->setText(QCoreApplication::translate("MainWindow", "Nombre de thread max possible:", nullptr));
        runThreadsMatrixButton->setText(QCoreApplication::translate("MainWindow", "lancer test", nullptr));
        label_5->setText(QCoreApplication::translate("MainWindow", "Test en cours...", nullptr));
        runThreadsVectorButton->setText(QCoreApplication::translate("MainWindow", "lancer test", nullptr));
        label_24->setText(QCoreApplication::translate("MainWindow", "Test en cours...", nullptr));
        MainWidgetTab->setTabText(MainWidgetTab->indexOf(demoThreadingTab), QCoreApplication::translate("MainWindow", "Demo Threading", nullptr));
        menuMenu->setTitle(QCoreApplication::translate("MainWindow", "Menu", nullptr));
        toolBar->setWindowTitle(QCoreApplication::translate("MainWindow", "toolBar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

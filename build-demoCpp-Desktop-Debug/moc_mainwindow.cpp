/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[18];
    char stringdata0[472];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 28), // "on_runFractionButton_clicked"
QT_MOC_LITERAL(2, 40, 0), // ""
QT_MOC_LITERAL(3, 41, 26), // "on_runMatrixButton_clicked"
QT_MOC_LITERAL(4, 68, 26), // "on_runVectorButton_clicked"
QT_MOC_LITERAL(5, 95, 29), // "on_Taille_Vect_A_valueChanged"
QT_MOC_LITERAL(6, 125, 29), // "on_Taille_Vect_B_valueChanged"
QT_MOC_LITERAL(7, 155, 30), // "on_getMaxThreadsButton_clicked"
QT_MOC_LITERAL(8, 186, 36), // "on_timeOutMatrixSpinBox_value..."
QT_MOC_LITERAL(9, 223, 36), // "on_timeOutVectorSpinBox_value..."
QT_MOC_LITERAL(10, 260, 33), // "on_runThreadsMatrixButton_cli..."
QT_MOC_LITERAL(11, 294, 33), // "on_runThreadsVectorButton_cli..."
QT_MOC_LITERAL(12, 328, 21), // "retrieveThreadsMatrix"
QT_MOC_LITERAL(13, 350, 19), // "std::map<int,float>"
QT_MOC_LITERAL(14, 370, 3), // "res"
QT_MOC_LITERAL(15, 374, 21), // "retrieveThreadsVector"
QT_MOC_LITERAL(16, 396, 37), // "on_displayThreadsMatrixButton..."
QT_MOC_LITERAL(17, 434, 37) // "on_displayThreadsVectorButton..."

    },
    "MainWindow\0on_runFractionButton_clicked\0"
    "\0on_runMatrixButton_clicked\0"
    "on_runVectorButton_clicked\0"
    "on_Taille_Vect_A_valueChanged\0"
    "on_Taille_Vect_B_valueChanged\0"
    "on_getMaxThreadsButton_clicked\0"
    "on_timeOutMatrixSpinBox_valueChanged\0"
    "on_timeOutVectorSpinBox_valueChanged\0"
    "on_runThreadsMatrixButton_clicked\0"
    "on_runThreadsVectorButton_clicked\0"
    "retrieveThreadsMatrix\0std::map<int,float>\0"
    "res\0retrieveThreadsVector\0"
    "on_displayThreadsMatrixButton_clicked\0"
    "on_displayThreadsVectorButton_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x0a /* Public */,
       3,    0,   85,    2, 0x0a /* Public */,
       4,    0,   86,    2, 0x0a /* Public */,
       5,    0,   87,    2, 0x0a /* Public */,
       6,    0,   88,    2, 0x0a /* Public */,
       7,    0,   89,    2, 0x0a /* Public */,
       8,    0,   90,    2, 0x0a /* Public */,
       9,    0,   91,    2, 0x0a /* Public */,
      10,    0,   92,    2, 0x0a /* Public */,
      11,    0,   93,    2, 0x0a /* Public */,
      12,    1,   94,    2, 0x0a /* Public */,
      15,    1,   97,    2, 0x0a /* Public */,
      16,    0,  100,    2, 0x0a /* Public */,
      17,    0,  101,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void, 0x80000000 | 13,   14,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        (void)_t;
        switch (_id) {
        case 0: _t->on_runFractionButton_clicked(); break;
        case 1: _t->on_runMatrixButton_clicked(); break;
        case 2: _t->on_runVectorButton_clicked(); break;
        case 3: _t->on_Taille_Vect_A_valueChanged(); break;
        case 4: _t->on_Taille_Vect_B_valueChanged(); break;
        case 5: _t->on_getMaxThreadsButton_clicked(); break;
        case 6: _t->on_timeOutMatrixSpinBox_valueChanged(); break;
        case 7: _t->on_timeOutVectorSpinBox_valueChanged(); break;
        case 8: _t->on_runThreadsMatrixButton_clicked(); break;
        case 9: _t->on_runThreadsVectorButton_clicked(); break;
        case 10: _t->retrieveThreadsMatrix((*reinterpret_cast< const std::map<int,float>(*)>(_a[1]))); break;
        case 11: _t->retrieveThreadsVector((*reinterpret_cast< const std::map<int,float>(*)>(_a[1]))); break;
        case 12: _t->on_displayThreadsMatrixButton_clicked(); break;
        case 13: _t->on_displayThreadsVectorButton_clicked(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

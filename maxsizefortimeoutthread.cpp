#include "maxsizefortimeoutthread.h"
#include "threads.h"
#include <QDebug>


MaxSizeForTimeOutThread::MaxSizeForTimeOutThread(int typeThread, float timeout, int max_thread)
{
    _typeThread=typeThread;
    _timeout=timeout;
    _max_thread=max_thread;
}

void MaxSizeForTimeOutThread::run(){
    switch (_typeThread) {
        case 1:{
            qDebug()<<"Go get_nb_size_tested_for_timeout Matrice!";
            auto ResultsMatrix = get_nb_size_tested_for_timeout(&thread_demo_matrix,_timeout,_max_thread-1); // _max_thread-1 car 1 utilisé pour ce QThread
            emit ThreadsMatrixResultsReady(ResultsMatrix);
            break;
        }
        case 2:{
            qDebug()<<"Go get_nb_size_tested_for_timeout Vector!";
            auto ResultsVector = get_nb_size_tested_for_timeout(&thread_demo_vector,_timeout,_max_thread-1); // _max_thread-1 car 1 utilisé pour ce QThread            emit ThreadsVectorResultsReady(ResultsVector);
            emit ThreadsVectorResultsReady(ResultsVector);
            break;
        }
        default:{
            throw std::invalid_argument("Valeur choix thread impossible");
        }
    };
}

#ifndef MAXSIZEFORTIMEOUTTHREAD_H
#define MAXSIZEFORTIMEOUTTHREAD_H

#include <QtCore>
#include <map>

using namespace std;

//!  QThread pour libérer la fenêtre principale
/*!
  QThread permettant le lancer la fonction get_nb_size_tested_for_timeout du fichier "threads.h"
  afin de libérer la fenêtre principale du programme et éviter les freezes lors des lancements
  des démos de multithreading
*/

class MaxSizeForTimeOutThread : public QThread
{
    Q_OBJECT
    public:
        //! constructeur
        /*!
         * \brief MaxSizeForTimeOutThread
         * \param typeThread 1 pour Matrice, 2 pour Vecteur
         * \param timeout limite de temps à ne pas dépasser par les threads (en s)
         * \param max_thread Nombre de threads concurents possible (dépend généralement du nombre de processeurs dispo)
         */
        MaxSizeForTimeOutThread(int typeThread, float timeout, int max_thread);

        //! Lanceur
        void run();

    signals:
        //! Signals émit lorsque MaxSizeForTimeOutThread a terminé, configuré pour des tests Matrice
        void ThreadsMatrixResultsReady(const map<int, float> &res);

        //! Signals émit lorsque MaxSizeForTimeOutThread a terminé, configuré pour des tests Vecteurs
        void ThreadsVectorResultsReady(const map<int, float> &res);

    private:
        //! Définit le type de thread: 1= Matrix;  2=Vector
        int _typeThread; // 1= Matrix;  2=Vector
        //! Définit le timeout (en s)
        float _timeout;
        //! Définit le nombre max de thread concurrent utilisable
        int _max_thread;
};

#endif // MAXSIZEFORTIMEOUTTHREAD_H

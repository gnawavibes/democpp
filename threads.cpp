#include "threads.h"

#include <thread>
#include <mutex>
#include <future>
#include <chrono>
#include <exception>
#include <functional>
#include <assert.h>

#include "include/Fraction.h"
#include "include/FractMatrix.h"
#include "include/FractVector.h"
#include <map>


    using namespace std;
    constexpr auto cnow = chrono::high_resolution_clock::now;
    std::mutex mtx; // mutex to lock acces to clock
    std::chrono::time_point<chrono::high_resolution_clock> start;


map<int, float> get_nb_size_tested_for_timeout(std::function<void(int,std::promise<float>&)>f, float timeout, int max_thread){
    //initialisation: création d'un Pool de thread + allocation mémoire pour promises et futur correspondant à chaque thread
    // + création de variables utiles
    map<int, float> results;
    int nb_size_tested = 1;
    bool flag = true;
    chrono::duration<float> duree; // CHRONO
    std::chrono::time_point<chrono::high_resolution_clock> check;
    std::promise<float> promises[max_thread];
    std::future<float> futures[max_thread];
    std::thread Pool[max_thread];
    // Boucle tant qu'on a pas trouvé à partir de quelle taille de vecteur/matrice
    // on ne tient plus le timeout
    while(flag){
        // on active des paires promesses/futurs pour signaler la fin des threads
        for(int x=0;x<max_thread;++x){
            promises[x]=std::promise<float>();
            futures[x]=promises[x].get_future();
        }
        // création des threads
        for(int x=0;x<max_thread;++x){
            Pool[x]= std::thread(f, x+nb_size_tested,std::ref(promises[x]));
        }
        // On chronomtère le temps du timeout
        mtx.lock();
        start = cnow();
        mtx.unlock();
        do{
            check = cnow();
            duree = check-start;
        }while(duree.count()<timeout);
        cout<<"TIMEOUT! temps écoulé: "<< duree.count()<<"s"<<endl;
        // On regarde les threads qui ont finit avant le timeout
        for (int x=0; x<max_thread;++x){
            auto status = futures[x].wait_for(std::chrono::milliseconds(0));
            if(status == std::future_status::ready){
                cout << "thread de taille=" << x+nb_size_tested << " a finit avant le timeout."<<endl;
                //nb_size_tested++;
            }
            else{
                cout << "thread de taille=" << x+nb_size_tested << " status:"<< (status == std::future_status::ready)<<endl;
                flag = false;
                break;
            }
        }
        // on termine tous les threads
        for (int x=0; x<max_thread;++x){
            Pool[x].join();
            results[x+nb_size_tested]=futures[x].get();
        }
        nb_size_tested+=max_thread;
    }
    return results;
}

void demo_fraction(){
    Fraction a(5,25), b(2,4), c(2), d;
    cout << "a=" << a << " b=" << b << " c=" << c << " d=" << d <<endl;
    a.reduce();
    cout << "a reduit = " << a <<endl;
    d.set_den(7);
    d.set_num(4);
    cout << "d = " << a <<endl;
    cout << "a + b = " << d << "  a - b = " << a-b << "  a * b = " << a*b << "  a / b = " << a/b << endl;
}

void demo_matrix(int Msize, bool display){
    FractMatrix A(Msize,Msize), B(Msize,Msize);
    A.FillRandomly();
    B.FillRandomly();
    FractMatrix AT = A.T();
    FractMatrix BT = B.T();
    FractMatrix ApB = A+B;
    FractMatrix AmB = A-B;
    FractMatrix AB = A*B;
    if (display){
        cout << "A= " << A <<endl;
        cout << "B= " << B <<endl;
        cout << "A.T= " << AT <<endl;
        cout << "B.T= " << BT <<endl;
        cout << "A+B= " << ApB <<endl;
        cout << "A-B= " << AmB <<endl;
        cout << "A*B= " << AB <<endl;
    }
}

void demo_vector(int Vsize, bool display){
    FractVector V1(Vsize), V2(Vsize) ;
    V1.FillRandomly();
    V2.FillRandomly();
    FractMatrix V1T = V1.T();
    FractMatrix V1pV2 = V1+V2;
    FractMatrix V1mV2 = V1-V2;
    Fraction V1dotV2 = scalarProduct(V1,V2);
    if (display){
        cout << "V1= " << V1 <<endl;
        cout << "V1.T=" << V1T << endl;
        cout << "V2= " << V2 <<endl;
        cout << "V1+V2= " << V1pV2 <<endl;
        cout << "V1-V2= " << V1mV2 <<endl;
        cout << "V1.V2= " << V1dotV2 <<endl;
    }
    try{
        FractMatrix V1V2 = V1*V2;
        if (display)
            cout << "V1*V2= " << V1V2 << endl<<endl;
    }
    catch(...){
        FractMatrix V1TV2 = V1.T()*V2;
        FractMatrix V1V2T = V1*V2.T();
        if (display){
            cout << "Avez vous essayé de transposer les vecteurs?" << endl;
            cout << "V1.T*V2= " << V1TV2 << endl;
            cout << "V1*V2.T= " << V1V2T << endl<<endl;
        }
    }
    FractVector V3(3), V4(3), V3pV4(3);
    V3.FillRandomly();
    V4.FillRandomly();
    FractVector V3prodV4 = (V3 ^ V4);
    if (display)
        cout << "V3^V4= " << V3prodV4 <<endl;
}

void thread_demo_matrix(int Msize, std::promise<float>&p){
    cout << "Début thread Matrix de taille: " <<Msize<<endl;
    demo_matrix(Msize,false);
    cout << "Fin thread Matrix de taille: " <<Msize<<endl;
    mtx.lock();
    p.set_value((cnow()-start).count());
    mtx.unlock();
}

void thread_demo_vector(int Vsize, std::promise<float>&p){
    cout << "Début thread Vector de taille: " <<Vsize<<endl;
    demo_vector(Vsize, false);
    cout << "Fin thread Vector de taille: " <<Vsize<<endl;
    mtx.lock();
    p.set_value((cnow()-start).count());
    mtx.unlock();
}
